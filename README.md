# tinayiluo_IDS721_week10

[![pipeline status](https://gitlab.com/ly178/tinayiluo_ids721_week10/badges/main/pipeline.svg)](https://gitlab.com/ly178/tinayiluo_ids721_week10/-/commits/main)

# Rust Serverless Transformer Endpoint

This project demonstrates setting up a serverless Rust application on AWS Lambda that utilizes the [Hugging Face Transformer model](https://huggingface.co/rustformers/pythia-ggml/tree/main) (`rustformers/pythia-ggml`). The application is containerized using Docker, and deployment is managed through AWS Lambda with a CI/CD pipeline configured via GitLab. The application, which is containerized using Docker, leverages AWS Lambda for deployment and incorporates a CI/CD pipeline configured through GitLab. Specifically, this endpoint is designed to perform sentence completion: users can input a phrase, and the model will generate a contextually relevant completion of the sentence. This functionality makes it a versatile tool for various applications, such as writing assistance, chatbots, and more.

## Prerequisites

- AWS CLI
- Docker (with ARM64 support)
- Rust and Cargo
- Git and Git LFS

## Technologies Used

- Rust
- Docker
- AWS Lambda
- GitLab CI/CD
- Hugging Face Transformers (`rustformers`)

## Setup and Deployment Instructions

### 0. Install Necessary Tools

Before starting with the project setup, ensure that you have all necessary tools installed, including Git Large File Storage (Git LFS) for managing large model files:

```bash
brew install git-lfs
git lfs install
```
After installing Git LFS, you need to specify which files should be tracked using LFS rather than standard Git. This is particularly important for the binary model files, which are typically too large to be handled efficiently by regular Git operations. Use the following command to track `.bin` files with Git LFS since the model being used in this project is downloaded as `pythia-1b-q4_0-ggjt.bin` in the `src` folder:

```bash
git lfs track "*.bin"
```

Make sure to run this command inside your project directory. It sets up Git LFS to track all `.bin` files that you add to your repository, which helps in efficient handling of large files by storing references in the repository, rather than the files themselves.

### 1. Clone the Repository

Clone the repository and navigate to the project directory:
```bash
git clone <repository-url>
cd <repository-directory>
```

### 2. Local Testing and Development

#### Using the Model Locally

The `main.rs` file includes Rust code for setting up and querying the transformer model. Here’s a breakdown:

- **Model Setup**: The model and tokenizer are initialized with the specified architecture and path. The model is loaded with `llm::load_dynamic` which dynamically loads the model based on the provided architecture (`GptNeoX`).
- **Query Handling**: The `function_handler` extracts the query from the HTTP request and passes it to the `infer` function. The infer function generates a response using the model and returns this as the HTTP response.

```rust
// Example of handling a request and performing inference
async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    let query = event.query_string_parameters_ref()
        .and_then(|params| params.first("query"))
        .unwrap_or("Default query");
    let response = infer(query.to_string()).unwrap_or_else(|err| format!("Error: {:?}", err));
    let resp = Response::builder()
        .status(200)
        .header("content-type", "text/html")
        .body(response.into())
        .map_err(Box::new)?;
    Ok(resp)
}
```

#### Testing Locally

Use `cargo lambda watch` to compile the application and run it locally, emulating the Lambda environment. You can use Postman or `curl` to send requests to the local endpoint.

## Setup and Deployment Instructions

### 3. Deploying the Model to AWS

#### Configure Environment Variables

Before building and deploying the Docker container, you'll need to ensure that your AWS credentials and other necessary environment variables are correctly set. This involves creating a `.env` file at the root of your project directory with all the necessary environment variables defined. Here’s a template example for your `.env` file:

```
AWS_ACCESS_KEY_ID=your_access_key_id_here
AWS_SECRET_ACCESS_KEY=your_secret_access_key_here
AWS_REGION=your_aws_region_here
```

Once your `.env` file is configured, use the following commands in your terminal to export these variables into your session. This step ensures that all subsequent commands that require AWS credentials can authenticate properly.

```bash
set -a  # Automatically export all variables
source .env
set +a  # Stop automatically exporting
```

These commands are crucial for the proper deployment of your Docker container to AWS ECR and Lambda, as they ensure that your AWS CLI commands authenticate using the correct credentials.

#### Docker Configuration

Build the Docker image with the following command:
```bash
docker buildx build --progress=plain --platform linux/arm64 -t week10 .
```

Push the built image to your ECR repository:
```bash
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin <ecr-repository-url>
docker tag week10:latest <ecr-repository-url>/week_10:latest
docker push <ecr-repository-url>/week_10:latest
```

#### Lambda Deployment

Create a new Lambda function using the AWS Management Console. Select "Container image" as the source and configure memory and timeout settings to accommodate the model's requirements (change memory to the max limit of 3008 & change timeout to the max of 15 minutes)

### 4. CI/CD Pipeline Using GitLab and Makefile

#### GitLab CI/CD Configuration

The `.gitlab-ci.yml` file defines stages for building, testing, and deploying the application:

- **Build and Test**: The pipeline performs linting, formatting, and compilation of the Rust code.
- **Deployment**: The Docker image is built and pushed to ECR, and updates are pushed to the AWS Lambda function.

Example stage in `.gitlab-ci.yml`:
```yaml
build-test:
  image: ubuntu:latest
  script:
    - cargo lambda build
    - make test
```

#### Makefile Usage

The `Makefile` includes commands for building, testing, and linting the application:
```makefile
build: 
	cargo lambda build --release 

test:
	cargo test --quiet
```

These commands are referenced in the GitLab CI configuration to ensure that each merge request or push triggers the appropriate workflows.

## Endpoint Functionality 

**Example Usage**

The application enables users to input a phrase, and it will generate a complete, contextually appropriate sentence. For instance:

**Input:**
"I feel lonely because"

**Output:**
"I feel lonely because I've been without my partner for too long, and have some sort of conflict."

This example showcases the endpoint's ability to extend an incomplete thought into a coherent and contextually enriched sentence, highlighting the successful functionality of this serverless Rust application.

## Deliverables

- Postman Local Testing
![Screen_Shot_2024-04-13_at_5.01.19_PM](/uploads/1dd81b9877553bee34c85644ee002ae8/Screen_Shot_2024-04-13_at_5.01.19_PM.png)

- AWS Lambda configuration
![Screen_Shot_2024-04-13_at_6.39.33_PM](/uploads/5ddc4d4b8e2d04222eaeb7aae2073c20/Screen_Shot_2024-04-13_at_6.39.33_PM.png)

- Function URL: https://spb6x3md3bzwbjrhfhsqfdip4y0wzzip.lambda-url.us-east-1.on.aws/
![Screen_Shot_2024-04-13_at_6.28.01_PM](/uploads/f8d914350c5f72ce824fdba7226e1625/Screen_Shot_2024-04-13_at_6.28.01_PM.png)

